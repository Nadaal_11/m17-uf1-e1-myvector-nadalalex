﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m17_uf1_e1_myvector_nadalalex
{
    internal class VectorGame
    {
        public MyVector[] RandomVector(int allargada)
        {
            MyVector[] vector = new MyVector[allargada];
            Random random = new Random();
            for (int i = 0; i < allargada; i++)
            {
                vector[i] = new MyVector(new int[2] { random.Next(1, 50), random.Next(1, 50) }, new int[2] {random.Next(1, 50), random.Next(1, 50)});
            }
            return vector;
        }
        public MyVector[] SortVectors(MyVector[] arrayVectors, bool ordre)
        {
            List<MyVector> ordenat;
            if(ordre) ordenat = arrayVectors.ToList().OrderBy(vector => vector.GetDistancia()).ToList();
            else ordenat = arrayVectors.ToList().OrderBy(vector => vector.ProxOrigen()).ToList();
            return ordenat.ToArray<MyVector>();
        }
    }
}
