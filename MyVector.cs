﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m17_uf1_e1_myvector_nadalalex
{
    internal class MyVector
    {
        private int[] _final = new int[2];
        private int[] _inici = new int[2];
        public MyVector(int[] final, int[] inici)
        {
            _final = final;
            _inici = inici;
        }
        public MyVector()
        {
            _final = new int[] { 0, 0 };
            _inici = new int[] { 0, 0 };
        }
        public void SetInici(int num1, int num2)
        {
            _inici[0] = num1;
            _inici[1] = num2;
        }
        public void SetFinal(int num1, int num2)
        {
            _final[0] = num1;
            _final[1] = num2;
        }
        public int[] GetFinal() => _final;
        public int[] GetInici() => _inici;

        public double GetDistancia()
        {
            int c1 = _inici[0] - _final[0];
            int c2 = _inici[1] - _final[1];
            return Math.Sqrt(Math.Pow(c1, 2) + Math.Pow(c2, 2));
        }
        public double ProxOrigen()
        {
            double resultat1 = Math.Sqrt(_inici[0] * _inici[0] + _inici[1] * _inici[1]);
            if (Math.Sqrt(_final[0] * _final[0] + _final[1] * _final[1]) < resultat1) resultat1 = Math.Sqrt(_final[0] * _final[0] + _final[1] * _final[1]);
            return resultat1;
        }

        public void CanviarSentit()
        {
            (_inici, _final) = (_final, _inici);
        }

        public override string ToString()
        {
            return "El vector comença al punt: " + _inici[0] + ","+_inici[1] + " i acaba al: " + _final[0] + "," + _final[1];
        }

    }
}
