﻿using m17_uf1_e1_myvector_nadalalex;

internal class Program
{
    public static void Main(string[] args)
    {
        var menu = new Menu();
        menu.Inici();
    }


    public void GetName()
    {
        Console.WriteLine("Introdueix el teu nom: ");
        Console.WriteLine("Hello, " + Console.ReadLine());
    }
    public void Vectors()
    {
        MyVector vector = new MyVector();
        Console.WriteLine("Creem un vector buit: ");
        Console.WriteLine(vector);
        Console.WriteLine("Posem un inici i un final: ");
        vector.SetInici(1, 2);
        vector.SetFinal(3, 5);
        Console.WriteLine(vector);
        Console.WriteLine("La distancia del vector és: "+vector.GetDistancia());
        Console.WriteLine("Si canviem el sentit del vector quedaria tal que així: ");
        vector.CanviarSentit();
        Console.WriteLine(vector);
    }
    public void RandomVectors()
    {
        Console.WriteLine("Creem una llista de 10 MyVectors aleatoris: ");
        Console.WriteLine();
        VectorGame vectorGame = new VectorGame();
        MyVector[] lista = vectorGame.RandomVector(10);
        for (int i = 0; i <lista.Length; i++)
        {
            Console.WriteLine(lista[i]);
        }
    }
    public void SortVectors()
    {
        Console.WriteLine("Creem una llista de 10 MyVectors aleatoris: ");
        Console.WriteLine();
        VectorGame vectorGame = new VectorGame();
        MyVector[] lista = vectorGame.RandomVector(10);
        for (int i = 0; i < lista.Length; i++)
        {
            Console.WriteLine(lista[i]);
        }
        Console.WriteLine("Ens disposem a ordenaro per distancia: ");
        lista = vectorGame.SortVectors(lista, true);
        for (int i = 0; i < lista.Length; i++)
        {
            Console.WriteLine(lista[i]+". La distància és: " + lista[i].GetDistancia());
        }
        Console.WriteLine("Ens disposem a ordenaro per proximitat a origen: ");
        lista = vectorGame.SortVectors(lista, false);
        for (int i = 0; i < lista.Length; i++)
        {
            Console.WriteLine(lista[i] + ". La proximitat al origen és: " + lista[i].ProxOrigen());
        }
    }

}