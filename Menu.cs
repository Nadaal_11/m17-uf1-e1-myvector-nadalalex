﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m17_uf1_e1_myvector_nadalalex
{
    internal class Menu
    {
        public void Inici()
        {
            bool fi;
            do
            {
                MostrarMenu();
                fi = TractarOpcio();
            } while (!fi);
        }
        private void MostrarMenu()
        {
            Console.WriteLine("M17-UF1-E1");
            Console.WriteLine("----------");
            Console.WriteLine("[1] Hello{Name}");
            Console.WriteLine("[2] Vectors");
            Console.WriteLine("[3] Random Vectors");
            Console.WriteLine("[4] Sort Vectors");
            Console.WriteLine("[5] Finalitzar execució");
        }
        private bool TractarOpcio()
        {
            Program program = new Program();
            MyVector myVector = new MyVector();

            bool finalitzacio;
            do
            {
                finalitzacio = true;
                var opcio = Console.ReadLine();
                if (opcio != "5") Console.Clear();
                switch (opcio)
                {
                    case "1":
                        program.GetName();
                        break;
                    case "2":
                        program.Vectors();
                        break;
                    case "3":
                        program.RandomVectors();
                        break;
                    case "4":
                        program.SortVectors();
                        break;
                    case "5":            
                        return true;
                        break;
                    default:
                        Console.WriteLine("Opció incorrecta!");
                        finalitzacio = false;
                        break;
                }
            } while (!finalitzacio);

            Console.WriteLine("\n\n     -- Press any key to return to the menu");
            Console.ReadKey();
            Console.Clear();
            return false;
        }

    }
}
